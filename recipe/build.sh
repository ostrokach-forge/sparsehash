#!/usr/bin/env bash

set -e

# Linux-specific
export CXXFLAGS="${CXXFLAGS} -DBOOST_MATH_DISABLE_FLOAT128"

# Common
export CPPFLAGS="${CPPFLAGS} -I${PREFIX}/include"
export LDFLAGS="${LDFLAGS} -L${PREFIX}/lib"
export CFLAGS="${CFLAGS} -m${ARCH}"
export CXXFLAGS="${CXXFLAGS} -m${ARCH}"

./configure --prefix=${PREFIX}
make
make check
make install
